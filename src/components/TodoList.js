import React, { Component } from 'react';
import TodoItem from "./TodoItem";

export class TodoList extends Component {
	render() {
		console.log("props in TodoList.js");
		console.log(this.props);
		const {items,clearList,handleDelete,handleEdit} = this.props;
		const listItems= items.map(item => {
							return (
								
								<TodoItem 
								key ={item.id} 
								title = {item.title}
								handleDelete = {()=>handleDelete(item.id)} 
								handleEdit = {()=>handleEdit(item.id)} 
								/>
								

								)
							//return <li>{item.title}</li>
						})
		return (
			
			<div>
				<ul className = "list-group my-5">
					<h3 className = "text-capitalize text-center"> ToDo List</h3>
					{listItems}
					<button 
					type="button" 
					className="btn btn-warning btn-block tetx-captialize mt-5"
					onClick = {clearList}
					>
					Clear List
					</button>
				</ul>
			</div>
		);
	}
}
export default TodoList;
