import React, { Component } from 'react';

export class TodoItem extends Component {
	render() {
		const {title,handleDelete,handleEdit} = this.props;
		return (
			<li className = "list-group-item text-capitalize d-flex justify-content-between my-2">
				<h6>{title}</h6>
				<div className = "todo-icon">
					<button type="button" 
						className = "mx-2 text-success"
						onClick ={handleEdit}
						>Sửa</button>

					// <span className = "mx-2 text-success" onClick ={handleEdit}>
					// 	<i className = "fas fa-pen" />
					// </span>
					<button type="button" 
						className = "mx-2 text-danger"
						onClick ={handleDelete}
						>Xóa</button>

					// <span className = "mx-2 text-danger" /*onClick ={handleDelete}*/>
					// 	<i className = "fas fa-trash" />
					// </span>
				</div>
			</li>	
		);
	}
}
export default TodoItem;
