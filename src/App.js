import React, { Component } from 'react';
import './App.css';
import TodoInput from "./components/TodoInput";
import TodoItem from "./components/TodoItem";
import TodoList from "./components/TodoList";
import uuid from "uuid";
import axios from 'axios';

class App extends Component {
	
	state = {
		items : [],
		id : uuid(),
		item :"",
		editItem :false
	};

	handleChange =(event) =>{
		this.setState({
			item : event.target.value
		});
	};

	handleSubmit = (event) =>{
		event.preventDefault();

		const newItem = {
			id  :this.state.id ,
			title :this.state.item
		}

		const updatedItems = [...this.state.items,newItem];
		// console.log('updatedItems value');
		// console.log(updatedItems);
		console.log("current title");
		console.log(this.state.item);

		//add new item to server
		// Send a POST request
		axios.post('http://todolist.api.webdevuit.com/todo-lists', {
			title: this.state.item
		})
		.then(function (response) {
			console.log('data from server');
			console.log(response);
		})
		.catch(function (error) {
			console.log('some error');
			console.log(error);
		});
		//add new item to brower view
		this.setState({
			items :updatedItems,
			item :'',
			id :uuid(),
			editItem :false
		});

		
		


	}

	clearList = ()=>{
		this.setState({
			item :[]
		})
	}

	handleDelete = (id) =>{
		console.log("id in delete");
		console.log(id);
		//delete on server
		let reqURL = 'http://todolist.api.webdevuit.com/todo-lists/';
		reqURL+=id;
		console.log("reqURL");
		console.log(reqURL);
		axios.delete(reqURL, {
			id: id
		})
		.then(function (response) {
			console.log('message from server over delete method');
			console.log(response);
		})
		.catch(function (error) {
			console.log('some error over delete method');
			console.log(error);
		});
		//delete on brower view

		const filteredItems = this.state.items.filter(item => item.id !== id)
		this.setState({
			items :filteredItems
		});
	}
	handleEdit = (id) => {
		
		// const filteredItems = this.state.items.filter(item => item.id !== id);
		// const selectedItem =this.state.items.find(item => item.id === id);
		// console.log('filteredItems in app ');
		// console.log(filteredItems);
		// console.log('selectedItem in app');
		// console.log(selectedItem);
		// console.log("id in app");
		// console.log(id);
		// this.setState({
		// 	items :filteredItems,
		// 	editItem :true,
		// 	id :id
		// });


		//-------------
		const filteredItems = this.state.items.filter(item => item.id !== id);
		const selectedItem =this.state.items.find(item => item.id === id);
		
		this.setState({
			items :filteredItems,
			editItem :true,
			id :id
		});
		let reqURL = 'http://todolist.api.webdevuit.com/todo-lists/';
		reqURL+=id;
		axios.patch(reqURL, {
			title: selectedItem.title
		})
		.then(function (response) {
			console.log('message from server over delete method');
			console.log(response);
		})
		.catch(function (error) {
			console.log('some error over delete method');
			console.log(error);
		});
	}

	componentWillMount() {
		let currentComponent = this;
		//get vailable data from server 
		// Make a request for a user with a given ID
		axios.get('http://todolist.api.webdevuit.com/todo-lists')
		.then(function (response) {
    		// handle success
    		console.log('data from server by get method');
    		console.log(response.data);
    		// //add new item to brower view
    		currentComponent.setState({
    			items :response.data,
    			item :'',
    			editItem :false
    		});
    	})
		.catch(function (error) {
    		// handle error
    		console.log('some error by get method');
    		console.log(error);
    	})
		.then(function () {
    		// always executed
    	});

		
	}
	render(){
		//console.log(1);
		return(
			<div className="container">
			<div className = "row">
			<div className = "col-10 mx-auto col-md-8 mt-4">
			<h3 className ="text-capitalize text-center">
			ToDo App
			</h3>
			<TodoInput 
			item ={this.state.item} 
			handleChange= {this.handleChange}
			handleSubmit= {this.handleSubmit}
			editItem ={this.state.editItem}
			/>
			<TodoList items = {this.state.items} 
			clearList = {this.clearList}
			handleDelete = {this.handleDelete}
			handleEdit = {this.handleEdit}
			/>
			</div>
			</div>
			</div>
			
			);
		}
	}

	export default App;
